<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
////    return view('welcome');
//
//    \Spatie\Browsershot\Browsershot::url('https://kennelunionofgh.com/print-full-cert/d2173810-68b2-11e8-bcdf-61559e2e0402')
//        ->save(public_path()."/images/demo2.png");
//});


Route::get("/{id}", "CertificationController@getIndex");
