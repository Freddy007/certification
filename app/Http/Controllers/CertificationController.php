<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Promise\Promise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Spatie\Browsershot\Browsershot;
use Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot;

class CertificationController extends Controller
{
    //
    public function getIndex(Request $request, $id){
//        $id = $request->query("id");
        $name = $request->query("name");


        try {
            Browsershot::url('https://kennelunionofghana.com/pdf-certificate/' . $id . "?name=".$name)
                ->windowSize(1800, 1292)
                ->save(public_path() . "/images/" . $id . ".png");

            $filepath = public_path() . "/images/" . $id . ".png";
            return Response::download($filepath);

        }catch(Exception $ex){

        return view("download-cert", compact("id"));

        }        
         
        return view("download-cert", compact('id'));
    }
}
